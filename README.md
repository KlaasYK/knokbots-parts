# Knonkbots parts

FreeCAD models for 3D printable parts for antweight battlebots.

## License

CERN Open Hardware Licence Version 2 - Permissive
