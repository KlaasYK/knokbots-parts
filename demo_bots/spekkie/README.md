# Spekkie - Antweight pusher bot

Spekkie is modelled after a diamond shaped marshmellow ("spekje" in Dutch).
It's a simple pusher bot weighing in at only 138 grams.

It features an integrated switch and 2 leds for determining orientation and is capable
of driving upside down if flipped.

It has a length of 175mm and a width of 125mm, so unfortunetaly it does not fit a 100x100mm square
used in some antweight competitions. See the size guide for a reference.

Don't like "spekjes"? Print the whipped cream and make it a piece of cheesecake.

## Print instructions

Make sure to include a color change around 15.50mm, the model is 31mm tall.

Prints without supports (flip the top piece). Requires minor post-processing by using a thin hex key
to punch through the screw holes.

## BOM

- Bag of "spekjes" for eating during printing and assembly
- 2x N20 motors (6v, 500RPM)
- 2x N20 compatible wheels with D slot (32mm diameter)
- 1x 2s-3s Dual channel brushed speedcontroller with 5V BEC
- 1x RC receiver (Flysky FS2A 4CH Afhds 2A)
- 1x 2S LiPO (500 mAh but smaller ones should work too; saves weight)
- 1x Switch (SS12F15)
- 3x M3 8mm hex bolts
- 3x M3 threaded inserts
- 7x 2.5mm x 100mm tie wraps (electronics)
- 1x 3.5mm x 200mm tie wrap (batterystrap)
- Some tape to fixate the RC antenna

Optional for LEDs:

- 2x 3mm LED (red and yellow)
- 2x 220 ohm 1/4 watt resistor (size depends on LED colour!)
- cable and heatschrink for connecting LEDS

## Modifying

A FreeCAD file is provided, so you can inspect and make changes to the model.
