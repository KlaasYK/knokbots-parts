$fn=100;

linear_extrude(height=50, twist=2 * 360, scale=0.01, slices=100)
translate([5,0,0]) circle(25);

translate([5,0,-2.5])
{
    intersection()
    {
        cube([60, 60, 5], center=true);
        scale([1.02, 1.02, 0.5])sphere(25);
    }
}
